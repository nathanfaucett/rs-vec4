vec4
=====

rust vector 4 functions

```rust
extern crate vec4;

fn main() {
    let a = [1, 1, 1, 1];
    let b = [1, 1, 1, 1];
    let mut out = vec4::new(0, 0, 0, 0);

    vec4::add(&mut out, &a, &b);

    assert_eq!(out, [2, 2, 2, 2]);
}
```
